from fractions import Fraction
from random import randint

def exp(p_list):
    # assume p_list is in form (x, p(x))
    l = [p[0]*p[1] for p in p_list]

    return sum(l)

def main():
    p_at_x = list()
    for i in range(1, 12):
        # print(f"{i-1}/{21-i}", (i-1)/(21-i))
        p_at_x.append((i, Fraction((i-1),(21-i))))

    p_at_px = list()
    total = Fraction(0)
    for p in p_at_x:
        res = (1-total) * (p[1])
        total += res
        p_at_px.append((p[0], res))

    print([float(p[1].__round__(4)) for p in p_at_px])
    print(round(float(exp(p_at_px)), 2))

def sim(rounds=1):
    results = [0 for _ in range(12)]
    for _ in range(0, rounds):
        # set up
        socks = list(range(10)) + list(range(10))
        picked = []

        # simulate until we get a match
        for n in range(1,12):
            pick = socks.pop(randint(0,len(socks)-1))

            if pick in picked:
                results[n] += 1  # add result to the list and exit the round
                break
            else:
                picked.append(pick)

    print([r for r in results])
    print([r/sum(results) for r in results])

if __name__ == "__main__":
    sim(10000)
    main()

"""
We want to be able to plug into the expected value formula, that is, to sum up each nth choice of sock by the probability that it is picked as the nth choice [n * p(n)].

To do this, we must find a formula for each p(n). Before picking n socks, there are 21-n left. This means the conditional probability of finding a match on the nth choice is (n-1)/(21-n) (lines 11-14). To figure out the actual probability that it is the nth choice, we need to multiply this probability by the probability that there hasn't already been a successful match before n. To do this, we keep a cumulative sum of the probability (line 20), and subtract it from 1 to find the inverse, and multiply by the conditional probablility.

After we have this list for each n, we send it to the expected value formula (line 24) and the answer pops out!
"""
