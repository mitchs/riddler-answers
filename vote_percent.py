def guess_a(n):
    a = 32 * n // 100
    while(a/n < .33):
        a += 1

    return a-1

def guess_b(n):
    b = 58 * n // 100
    while(b/n < .59):
        b += 1

    return b-1

def guess_c(n):
    c = 73 * n // 100
    while(c/n < .74):
        c += 1

    return c-1

def main_min(exit=False):
    for n in range(1, 101):
        a = guess_a(n)
        b = guess_b(n)
        c = guess_c(n)

        if a/n >= .32 and b/n >= .58 and c/n >= .73:
            print(a, n, a/n)
            print(b, n, b/n)
            print(c, n, c/n)
            print("The minimum value is", n)
            if exit:
                break

def main_min(exit=False):
    for n in range(1, 101):
        a = guess_a(n)
        b = guess_b(n)
        c = guess_c(n)

        if a/n >= .32 and b/n >= .58 and c/n >= .73:
            print(a, n, a/n)
            print(b, n, b/n)
            print(c, n, c/n)
            print("The minimum value is", n)
            if exit:
                break


def main_max(exit=False):
    for n in range(99, 0, -1):
        a = guess_a(n)
        b = guess_b(n)
        c = guess_c(n)

        if a/n >= .32 and b/n >= .58 and c/n >= .73:
            pass
        else:
            print(a, n, a/n)
            print(b, n, b/n)
            print(c, n, c/n)
            print("The maximum value it can't be is", n)
            if exit:
                break


if __name__ == "__main__":
    main_min(True)
    main_max(True)


"""
The greatest number the office can't have is n=88. To show this, first we establish that these results are possible for any integer n >= 100 by contradiction. Assume there doesn't exist some integer x for hundreths-place decimal d (a decimal .yz for y,z in range [0,9] - for our problem, we will take d to be the values .32, .58, and .73) such that x/n < d < d + 1/100 <= (x+1)/n. However, x/n < d implies x/n + 1/n < d + 1/n <= d + 1/100, so this is a contradiction, and there must exist x in the range [d, d+1/100].

Now that we have an upper bound on the maximal value it can't be, we only must check values less than 100, of which the first is 88 (main_max).

To figure out the least value, we iterate up to 100 starting from 1 (main_min).
"""
